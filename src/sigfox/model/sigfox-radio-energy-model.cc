/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * Author: Romagnolo Stefano <romagnolostefano93@gmail.com>
 */

#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/pointer.h"
#include "ns3/energy-source.h"
#include "sigfox-radio-energy-model.h"

namespace ns3 {
namespace sigfox {

NS_LOG_COMPONENT_DEFINE ("SigfoxRadioEnergyModel");

NS_OBJECT_ENSURE_REGISTERED (SigfoxRadioEnergyModel);

TypeId
SigfoxRadioEnergyModel::GetTypeId (void)
{
  static TypeId tid =
      TypeId ("ns3::SigfoxRadioEnergyModel")
          .SetParent<DeviceEnergyModel> ()
          .SetGroupName ("Energy")
          .AddConstructor<SigfoxRadioEnergyModel> ()
          .AddAttribute ("StandbyCurrentA", "The default radio Standby current in Ampere.",
                         DoubleValue (0.01), // idle mode = 1.4mA
                         MakeDoubleAccessor (&SigfoxRadioEnergyModel::m_idleCurrentA),
                         MakeDoubleChecker<double> ())
          .AddAttribute ("TxCurrentA", "The radio Tx current in Ampere.",
                         DoubleValue (0.050), // transmit at 0dBm = 28mA
                         MakeDoubleAccessor (&SigfoxRadioEnergyModel::m_txCurrentA),
                         MakeDoubleChecker<double> ())
          .AddAttribute ("RxCurrentA", "The radio Rx current in Ampere.",
                         DoubleValue (0.013), // receive mode = 11.2mA
                         MakeDoubleAccessor (&SigfoxRadioEnergyModel::m_rxCurrentA),
                         MakeDoubleChecker<double> ())
          .AddAttribute ("SleepCurrentA", "The radio Sleep current in Ampere.",
                         DoubleValue (0.0000035), // sleep mode = 1.5microA
                         MakeDoubleAccessor (&SigfoxRadioEnergyModel::m_sleepCurrentA),
                         MakeDoubleChecker<double> ())
          .AddAttribute ("TxCurrentModel", "A pointer to the attached tx current model.",
                         PointerValue (),
                         MakePointerAccessor (&SigfoxRadioEnergyModel::m_txCurrentModel),
                         MakePointerChecker<SigfoxTxCurrentModel> ())
/*          .AddTraceSource (
              "TotalEnergyConsumption", "Total energy consumption of the radio device.",
              MakeTraceSourceAccessor (&SigfoxRadioEnergyModel::m_totalEnergyConsumption),
              "ns3::TracedValueCallback::Double")*/
          .AddTraceSource ("SystemCurrent", "System Current of the radio device.",
                           MakeTraceSourceAccessor (&SigfoxRadioEnergyModel::m_systemcurrent),
                           "ns3::TracedValueCallback::Double");
  return tid;
}

SigfoxRadioEnergyModel::SigfoxRadioEnergyModel ()
{
  NS_LOG_FUNCTION (this);
  m_currentState = EndPointSigfoxPhy::SLEEP; // initially STANDBY
  m_nPendingChangeState = 0;
  m_isSupersededChangeState = false;
  m_energyDepletionCallback.Nullify ();
  m_source = NULL;
  // set callback for EndPointSigfoxPhy listener
  m_listener = new SigfoxRadioEnergyModelPhyListener;
  m_listener->SetChangeStateCallback (MakeCallback (&DeviceEnergyModel::ChangeState, this));
  // set callback for updating the tx current
  m_listener->SetUpdateTxCurrentCallback (
      MakeCallback (&SigfoxRadioEnergyModel::SetTxCurrentFromModel, this));
}

SigfoxRadioEnergyModel::~SigfoxRadioEnergyModel ()
{
  NS_LOG_FUNCTION (this);
  delete m_listener;
}

void
SigfoxRadioEnergyModel::SetEnergySource (Ptr<EnergySource> source)
{
  NS_LOG_FUNCTION (this << source);
  NS_ASSERT (source != NULL);
  m_source = source;
}

double
SigfoxRadioEnergyModel::GetTotalEnergyConsumption (void) const
{
  double totalEnergyConsumption = m_source->GetInitialEnergy() - m_source->GetRemainingEnergy();

  return totalEnergyConsumption;
}

EndPointSigfoxPhy::State
SigfoxRadioEnergyModel::GetCurrentState (void) const
{
  NS_LOG_FUNCTION (this);
  return m_currentState;
}

void
SigfoxRadioEnergyModel::SetEnergyDepletionCallback (SigfoxRadioEnergyDepletionCallback callback)
{
  NS_LOG_FUNCTION (this);
  if (callback.IsNull ())
    {
      NS_LOG_DEBUG ("SigfoxRadioEnergyModel:Setting NULL energy depletion callback!");
    }
  m_energyDepletionCallback = callback;
}

void
SigfoxRadioEnergyModel::SetEnergyRechargedCallback (SigfoxRadioEnergyRechargedCallback callback)
{
  NS_LOG_FUNCTION (this);
  if (callback.IsNull ())
    {
      NS_LOG_DEBUG ("SigfoxRadioEnergyModel:Setting NULL energy recharged callback!");
    }
  m_energyRechargedCallback = callback;
}

void
SigfoxRadioEnergyModel::SetEnergyChangedCallback (SigfoxRadioEnergyChangedCallback callback)
{
  NS_LOG_FUNCTION (this);
  if (callback.IsNull ())
    {
      NS_LOG_DEBUG ("SigfoxRadioEnergyModel:Setting NULL energy recharged callback!");
    }
  m_energyChangedCallback = callback;
}



void
SigfoxRadioEnergyModel::SetTxCurrentModel (Ptr<SigfoxTxCurrentModel> model)
{
  m_txCurrentModel = model;
}

void
SigfoxRadioEnergyModel::SetTxCurrentFromModel (double txPowerDbm)
{
  if (m_txCurrentModel)
    {
      m_txCurrentA = m_txCurrentModel->CalcTxCurrent (txPowerDbm);
    }
}

/**
 ª Whenever I change state:
 *  1) I select the new current
 *  2) I compute the energy spent until now (I call UpdateEnergySource on m_source)
 *  3) I change the current and state
 */
void
SigfoxRadioEnergyModel::ChangeState (int newState)
{
  NS_LOG_FUNCTION (this << newState);

  double xx = 0.0;

  // new state current
  switch (newState)
    {
    case EndPointSigfoxPhy::STANDBY:
      xx = m_idleCurrentA;
      break;
    case EndPointSigfoxPhy::TX:
      xx = m_txCurrentA;
      break;
    case EndPointSigfoxPhy::RX:
      xx = m_rxCurrentA;
      break;
    case EndPointSigfoxPhy::SLEEP:
      xx = m_sleepCurrentA;
      break;
    default:
      NS_FATAL_ERROR ("SigfoxRadioEnergyModel:Undefined radio state from state: " << m_currentState);
    }

  // update total energy consumption
  m_source->UpdateEnergySource ();

  m_nPendingChangeState++;

  // in case the energy source is found to be depleted during the last update, a callback might be
  // invoked that might cause a change in the Sigfox PHY state (e.g., the PHY is put into SLEEP mode).
  // This in turn causes a new call to this member function, with the consequence that the previous
  // instance is resumed after the termination of the new instance. In particular, the state set
  // by the previous instance is erroneously the final state stored in m_currentState. The check below
  // ensures that previous instances do not change m_currentState.
  if (!m_isSupersededChangeState)
    {
      // update current state & last update time stamp
      SetSigfoxRadioState ((EndPointSigfoxPhy::State) newState);
      m_systemcurrent = xx;
    }

  m_isSupersededChangeState = (m_nPendingChangeState > 1);

  m_nPendingChangeState--;
}

void
SigfoxRadioEnergyModel::HandleEnergyDepletion (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("SigfoxRadioEnergyModel:Energy is depleted!");
  // invoke energy depletion callback, if set.
  if (!m_energyDepletionCallback.IsNull ())
    {
      m_energyDepletionCallback ();
    }
}

void
SigfoxRadioEnergyModel::HandleEnergyChanged (void)
{
//  NS_LOG_FUNCTION (this);
  double energynow = GetTotalEnergyConsumption();
  NS_LOG_DEBUG ("SigfoxRadioEnergyModel::HandleEnergyChanged Energy changed! Now it is " << energynow << " Time is " << Simulator::Now().GetSeconds());
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("SigfoxRadioEnergyModel:Energy is recharged!");
  // invoke energy recharged callback, if set.
  if (!m_energyChangedCallback.IsNull ())
    {
      m_energyChangedCallback(energynow);
    }
}

void
SigfoxRadioEnergyModel::HandleEnergyRecharged (void)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("SigfoxRadioEnergyModel:Energy is recharged!");
  // invoke energy recharged callback, if set.
  if (!m_energyRechargedCallback.IsNull ())
    {
      m_energyRechargedCallback ();
    }
}

SigfoxRadioEnergyModelPhyListener *
SigfoxRadioEnergyModel::GetPhyListener (void)
{
  NS_LOG_FUNCTION (this);
  return m_listener;
}

/*
 * Private functions start here.
 */

void
SigfoxRadioEnergyModel::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  m_source = NULL;
  m_energyDepletionCallback.Nullify ();
}

double
SigfoxRadioEnergyModel::DoGetCurrentA (void) const
{
//  NS_LOG_FUNCTION (this);
  switch (m_currentState)
    {
    case EndPointSigfoxPhy::STANDBY:
      return m_idleCurrentA;
    case EndPointSigfoxPhy::TX:
      return m_txCurrentA;
    case EndPointSigfoxPhy::RX:
      return m_rxCurrentA;
    case EndPointSigfoxPhy::SLEEP:
      return m_sleepCurrentA;
    default:
      NS_FATAL_ERROR ("SigfoxRadioEnergyModel:Undefined radio state:" << m_currentState);
    }
}

void
SigfoxRadioEnergyModel::SetSigfoxRadioState (const EndPointSigfoxPhy::State state)
{
  NS_LOG_FUNCTION (this << state);
  m_currentState = state;
  std::string stateName;
  switch (state)
    {
    case EndPointSigfoxPhy::STANDBY:
      stateName = "STANDBY";
      break;
    case EndPointSigfoxPhy::TX:
      stateName = "TX";
      break;
    case EndPointSigfoxPhy::RX:
      stateName = "RX";
      break;
    case EndPointSigfoxPhy::SLEEP:
      stateName = "SLEEP";
      break;
    }
  NS_LOG_DEBUG ("SigfoxRadioEnergyModel:Switching to state: "
                << stateName << " at time = " << Simulator::Now ().GetSeconds () << " s");
}

// -------------------------------------------------------------------------- //

SigfoxRadioEnergyModelPhyListener::SigfoxRadioEnergyModelPhyListener ()
{
  NS_LOG_FUNCTION (this);
  m_changeStateCallback.Nullify ();
  m_updateTxCurrentCallback.Nullify ();
}

SigfoxRadioEnergyModelPhyListener::~SigfoxRadioEnergyModelPhyListener ()
{
  NS_LOG_FUNCTION (this);
}

void
SigfoxRadioEnergyModelPhyListener::SetChangeStateCallback (
    DeviceEnergyModel::ChangeStateCallback callback)
{
  NS_LOG_FUNCTION (this << &callback);
  NS_ASSERT (!callback.IsNull ());
  m_changeStateCallback = callback;
}

void
SigfoxRadioEnergyModelPhyListener::SetUpdateTxCurrentCallback (UpdateTxCurrentCallback callback)
{
  NS_LOG_FUNCTION (this << &callback);
  NS_ASSERT (!callback.IsNull ());
  m_updateTxCurrentCallback = callback;
}

void
SigfoxRadioEnergyModelPhyListener::NotifyRxStart ()
{
  NS_LOG_FUNCTION (this);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("SigfoxRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndPointSigfoxPhy::RX);
}

void
SigfoxRadioEnergyModelPhyListener::NotifyTxStart (double txPowerDbm)
{
  NS_LOG_FUNCTION (this << txPowerDbm);
  if (m_updateTxCurrentCallback.IsNull ())
    {
      NS_FATAL_ERROR ("SigfoxRadioEnergyModelPhyListener:Update tx current callback not set!");
    }
  m_updateTxCurrentCallback (txPowerDbm);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("SigfoxRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndPointSigfoxPhy::TX);
}

void
SigfoxRadioEnergyModelPhyListener::NotifySleep (void)
{
  NS_LOG_FUNCTION (this);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("SigfoxRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndPointSigfoxPhy::SLEEP);
}

void
SigfoxRadioEnergyModelPhyListener::NotifyStandby (void)
{
  NS_LOG_FUNCTION (this);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("SigfoxRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndPointSigfoxPhy::STANDBY);
}

/*
 * Private function state here.
 */

void
SigfoxRadioEnergyModelPhyListener::SwitchToStandby (void)
{
  NS_LOG_FUNCTION (this);
  if (m_changeStateCallback.IsNull ())
    {
      NS_FATAL_ERROR ("SigfoxRadioEnergyModelPhyListener:Change state callback not set!");
    }
  m_changeStateCallback (EndPointSigfoxPhy::STANDBY);
}

} // namespace sigfox
} // namespace ns3
