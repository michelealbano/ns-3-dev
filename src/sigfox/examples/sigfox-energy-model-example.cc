#include "ns3/names.h"
#include "ns3/end-point-sigfox-phy.h"
#include "ns3/gateway-sigfox-phy.h"
#include "ns3/end-point-sigfox-mac.h"
#include "ns3/gateway-sigfox-mac.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/pointer.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/sigfox-helper.h"
#include "ns3/node-container.h"
#include "ns3/mobility-helper.h"
#include "ns3/position-allocator.h"
#include "ns3/double.h"
#include "ns3/random-variable-stream.h"
#include "ns3/periodic-sender-helper.h"
#include "ns3/command-line.h"
#include "ns3/forwarder-helper.h"
#include "ns3/energy-module.h"
#include "ns3/sdc-energy-source-helper.h"
#include "ns3/sigfox-net-device.h"
#include "ns3/sigfox-radio-energy-model-helper.h"
#include <algorithm>
#include <ctime>
#include "ns3/periodic-sender.h"
#include <algorithm>
#include <ctime>
#include <fstream>
#include <iostream>

using namespace ns3;
using namespace sigfox;

NS_LOG_COMPONENT_DEFINE ("SigfoxEnergyModelExample");

int nDevices = 1;
int nGateways = 1;

const int day = 86400;
const int TotalTime = 220*day;
//const int TotalTime = 2000;//220*day;

// THESE ARE GLOBAL VARIABLES, IN THE SENSE THAT ARE IN COMMON BETWEEN ALL THE DEVICES
// I DO NOT THINK WE SHOULD USE THEM
uint32_t receivedpackets = 0;
uint32_t sendpackets = 0;
uint32_t numberofmeasurments = 0;

double initialBattery = 36000000;   // Charge in mAs, corresponding to 10,000 mAh
double initialEnergy = initialBattery * 3.3;   // Energy in J

// WE CAN STILL USE THE MEASUREMENT ENERGY, UNTIL WE DO NOT IMPLEMENT THAT AS AN APPLICATION LAYER. THIS VALUE REPRESENT THE ENERGY THAT ALL THE NODES CONSUMED
double EnergyConsumptionMeasurement = 0; // I keep the measurement energy consumption in here, since neither the energy model nor the energy source allow to remove energy




void
syscurrent (double y, double x)
{
  std::ofstream out ("CurrentGraph.txt", std::ios::app);
  out << (Simulator::Now ()).GetSeconds () << " " << y << std::endl;
  out << (Simulator::Now ()).GetSeconds () << " " << x << std::endl;
  out.close ();
}


//______________________Measuring value___________________________
void
Measure()
{
  //NS_LOG_UNCOND (Simulator::Now ().GetSeconds () << "All nodes consumed energy to measure a value");
  EnergyConsumptionMeasurement += 5.8*4.9;// Measure current * measure time
  //numberofmeasurments += 1;
  Simulator::Schedule (Seconds (60.0), &Measure);
}


void ReportEnergy(double energynow) {
  double TotalRemainingEnergy = initialEnergy - energynow - EnergyConsumptionMeasurement;
//fprintf(stderr, "energynow %f\n", energynow);
/*  NS_LOG_UNCOND (Simulator::Now ().GetSeconds ()
                 << "s Current remaining energy = " << TotalRemainingEnergy << "J");*/

  if (TotalRemainingEnergy <= 0) TotalRemainingEnergy = 0;
  std::ofstream out ("BatteryLevel.txt", std::ios::app);
  out << (Simulator::Now ()).GetSeconds () << " , " << TotalRemainingEnergy << "" << std::endl;
  out.close ();
}



int
main (int argc, char *argv[])
{
    
    LogComponentEnable ("SigfoxEnergyModelExample", LOG_LEVEL_ALL);
     LogComponentEnable("SigfoxPhy", LOG_LEVEL_ALL);
     LogComponentEnable("EndPointSigfoxPhy", LOG_LEVEL_ALL);
     LogComponentEnable("GatewaySigfoxPhy", LOG_LEVEL_ALL);
     LogComponentEnable("SigfoxMac", LOG_LEVEL_ALL);
     LogComponentEnable("EndPointSigfoxMac", LOG_LEVEL_ALL);
     LogComponentEnable("GatewaySigfoxMac", LOG_LEVEL_ALL);
     LogComponentEnable("SigfoxHelper", LOG_LEVEL_ALL);
     LogComponentEnable("SigfoxPhyHelper", LOG_LEVEL_ALL);
    LogComponentEnable("PeriodicSenderHelper", LOG_LEVEL_ALL);
     LogComponentEnable("PeriodicSender", LOG_LEVEL_ALL);
    // LogComponentEnable("NetworkScheduler", LOG_LEVEL_ALL);
    // LogComponentEnable("NetworkServer", LOG_LEVEL_ALL);
    // LogComponentEnable("NetworkStatus", LOG_LEVEL_ALL);
    // LogComponentEnable("NetworkController", LOG_LEVEL_ALL);
      LogComponentEnable ("SimpleEndPointSigfoxPhy", LOG_LEVEL_ALL);
     //LogComponentEnable ("PeriodicSender", LOG_LEVEL_ALL);
     //LogComponentEnable ("SigfoxRadioEnergyModel", LOG_LEVEL_ALL);
      //LogComponentEnable ("SdcEnergySource", LOG_LEVEL_ALL);
      LogComponentEnable ("Forwarder", LOG_LEVEL_ALL);
  /************************
  *  Create the channel  *
  ************************/

  NS_LOG_INFO ("Creating the channel...");

  // Create the sigfox channel object
  Ptr<LogDistancePropagationLossModel> loss = CreateObject<LogDistancePropagationLossModel> ();
  loss->SetPathLossExponent (3.76);
  loss->SetReference (1, 7.7);

  Ptr<PropagationDelayModel> delay = CreateObject<ConstantSpeedPropagationDelayModel> ();

  Ptr<SigfoxChannel> channel = CreateObject<SigfoxChannel> (loss, delay);

  /************************
  *  Create the helpers  *
  ************************/

  NS_LOG_INFO ("Setting up helpers...");

  MobilityHelper mobility;
  Ptr<ListPositionAllocator> allocator = CreateObject<ListPositionAllocator> ();
  allocator->Add (Vector (0, 0, 0));
  allocator->Add (Vector (0, 0, 0));
  mobility.SetPositionAllocator (allocator);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");

  // Create the SigfoxPhyHelper
  SigfoxPhyHelper phyHelper = SigfoxPhyHelper ();
  phyHelper.SetChannel (channel);

  // Create the SigfoxMacHelper
  SigfoxMacHelper macHelper = SigfoxMacHelper ();

  // Create the SigfoxHelper
  SigfoxHelper helper = SigfoxHelper ();

  //Create the ForwarderHelper
  ForwarderHelper forHelper = ForwarderHelper ();

  /************************
  *  Create End Devices  *
  ************************/

  NS_LOG_INFO ("Creating the end device...");

  // Create a set of nodes
  NodeContainer endDevices;
  endDevices.Create (nDevices);

  // Assign a mobility model to the node
  mobility.Install (endDevices);

  // Create the SigfoxNetDevices of the end devices
  phyHelper.SetDeviceType (SigfoxPhyHelper::EP);
  macHelper.SetDeviceType (SigfoxMacHelper::EP);
  NetDeviceContainer endDevicesNetDevices = helper.Install (phyHelper, macHelper, endDevices);

  /*********************
   *  Create Gateways  *
   *********************/

  NS_LOG_INFO ("Creating the gateway...");
  NodeContainer gateways;
  gateways.Create (nGateways);

  mobility.SetPositionAllocator (allocator);
  mobility.Install (gateways);

  // Create a netdevice for each gateway
  phyHelper.SetDeviceType (SigfoxPhyHelper::GW);
  macHelper.SetDeviceType (SigfoxMacHelper::GW);
  helper.Install (phyHelper, macHelper, gateways);

  /************************
   * Install Energy Model *
   ************************/

  SdcEnergySourceHelper sdcSourceHelper;
  SigfoxRadioEnergyModelHelper radioEnergyHelper;

  // configure energy source
  sdcSourceHelper.Set ("SdcEnergySourceInitialEnergyJ", DoubleValue (initialEnergy)); 
  sdcSourceHelper.Set ("SdcEnergySupplyVoltageV", DoubleValue (3.3));

  radioEnergyHelper.Set ("StandbyCurrentA", DoubleValue (0.0108));
  //radioEnergyHelper.Set ("TxCurrentA", DoubleValue (28000));
  radioEnergyHelper.Set ("TxCurrentA", DoubleValue (50));
  radioEnergyHelper.Set ("SleepCurrentA", DoubleValue (0.00235));
  radioEnergyHelper.Set ("RxCurrentA", DoubleValue (13));

  radioEnergyHelper.SetTxCurrentModel ("ns3::ConstantSigfoxTxCurrentModel", "TxCurrent", DoubleValue (50));

  // install source on EDs' nodes
  EnergySourceContainer sources = sdcSourceHelper.Install (endDevices);
  Names::Add ("/Names/EnergySource", sources.Get (0));

  // install device model
  DeviceEnergyModelContainer deviceModels =
      radioEnergyHelper.Install (endDevicesNetDevices, sources);



  /*********************************************************************************/

  Time appStopTime = Seconds (TotalTime);
  PeriodicSenderHelper appHelper = PeriodicSenderHelper ();
  appHelper.SetPeriod (Seconds (600)); //TotalTime));
  appHelper.SetPacketSize (87);
  //Ptr<RandomVariableStream> rv = CreateObjectWithAttributes<UniformRandomVariable> ( "Min", DoubleValue (10), "Max", DoubleValue (10));
  ApplicationContainer appContainer = appHelper.Install (endDevices);

  appContainer.Start (Seconds (0));
  appContainer.Stop (appStopTime);

  /**************
   * Get output *
   **************/

  Ptr<SdcEnergySource> sdcSourcePtr = DynamicCast<SdcEnergySource> (sources.Get (0));
  Ptr<DeviceEnergyModel> basicRadioModelPtr =
      sdcSourcePtr->FindDeviceEnergyModels ("ns3::SigfoxRadioEnergyModel").Get (0);
  NS_ASSERT (basicRadioModelPtr != NULL);
  basicRadioModelPtr->TraceConnectWithoutContext ("SystemCurrent", MakeCallback (&syscurrent));






  for (EnergySourceContainer::Iterator itsource = sources.Begin(); itsource != sources.End (); itsource++) {
	DeviceEnergyModelContainer demc = (*itsource)->FindDeviceEnergyModels("ns3::SigfoxRadioEnergyModel");
	for (DeviceEnergyModelContainer::Iterator itmodel = demc.Begin(); itmodel != demc.End (); itmodel++) {
		Ptr<SigfoxRadioEnergyModel> modelPtr = DynamicCast<SigfoxRadioEnergyModel>(*itmodel);
		modelPtr->SetEnergyChangedCallback(MakeCallback(&ReportEnergy));
	}
  }






  /****************
  *  Simulation  *
  ****************/

  Simulator::Stop (Seconds (TotalTime));

  NS_LOG_INFO ("Running simulation...");

  Simulator::Schedule (Seconds (60.0), &Measure);
  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}
