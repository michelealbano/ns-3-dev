# TODOs

- [x] Remove all traces of ~lorawan~ module
- [x] Adapt code to Sigfox (remove mentions of Spreading Factors and other LoRaWAN-specific things)
- [x] Clean up debug log messages
- [x] Remove magic numbers
- [x] Correctly compute duration of transmissions
- [ ] Add PHY layer abstraction
  - Set up available channels with appropriate frequencies
  - Implement frame error rate model in SigfoxInterferenceHelper
- [ ] Make sure no more magic numbers exist in the code
- [ ] Write documentation
- [ ] Add tests
- [ ] Update authors in all files
